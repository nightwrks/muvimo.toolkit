﻿using System;
using System.Collections.Generic;
using System.Linq;
using Muvimo.Toolkit.Messaging.Utility;

namespace Muvimo.Toolkit.Messaging
{
	/// <summary>
	/// 
	/// The Messenger is a singleton class allowing objects to exchange messages.
	/// 
	/// </summary>
	public class Messenger
	{
		private static Messenger _Singleton;
		private Dictionary<Type, List<WeakActionAndToken>> _Recipients;
		// --- SINGLETON ---------------------------------------------------------------------------------

		#region public static Messenger Singleton

		/// <summary>
		/// Gets the Messenger's default instance, allowing
		/// to register and send messages in a static manner.
		/// </summary>
		public static Messenger Singleton {
			get {
				if (_Singleton == null) 
				{
					_Singleton = new Messenger ();
				}
				return _Singleton;
			}
		}

		#endregion

		// --- CTOR / DISPOSAL ---------------------------------------------------------------------------------

		#region private void Cleanup()

		private void Cleanup ()
		{
			CleanupList (_Recipients);
		}

		#endregion

		#region public static void Reset()

		/// <summary>
		/// Sets the Messenger's default (static) instance to null.
		/// </summary>
		public static void Reset ()
		{
			_Singleton = null;
		}

		#endregion

		// --- PUBLIC METHODS ----------------------------------------------------------------------------
		// --- Registering

		#region public void Register<TMessage>(object recipient, Action<TMessage> action)

		/// <summary>
		/// Registers a recipient for a type of message TMessage. The action
		/// parameter will be executed when a corresponding message is sent.
		/// <para>Registering a recipient does not create a hard reference to it,
		/// so if this recipient is deleted, no memory leak is caused.</para>
		/// </summary>
		/// <typeparam name="TMessage">The type of message that the recipient registers
		/// for.</typeparam>
		/// <param name="recipient">The recipient that will receive the messages.</param>
		/// <param name="action">The action that will be executed when a message
		/// of type TMessage is sent.</param>
		public void Register<TMessage> (object recipient, Action<TMessage> action)
		{
			Register (recipient, null, action);
		}

		#endregion

		#region public void Register<TMessage>( object recipient, object token, Action<TMessage> action)

		/// <summary>
		/// Registers a recipient for a type of message TMessage.
		/// The action parameter will be executed when a corresponding 
		/// message is sent. See the receiveDerivedMessagesToo parameter
		/// for details on how messages deriving from TMessage (or, if TMessage is an interface,
		/// messages implementing TMessage) can be received too.
		/// <para>Registering a recipient does not create a hard reference to it,
		/// so if this recipient is deleted, no memory leak is caused.</para>
		/// </summary>
		/// <typeparam name="TMessage">The type of message that the recipient registers
		/// for.</typeparam>
		/// <param name="recipient">The recipient that will receive the messages.</param>
		/// <param name="token">A token for a messaging channel. If a recipient registers
		/// using a token, and a sender sends a message using the same token, then this
		/// message will be delivered to the recipient. Other recipients who did not
		/// use a token when registering (or who used a different token) will not
		/// get the message. Similarly, messages sent without any token, or with a different
		/// token, will not be delivered to that recipient.</param>
		/// <param name="action">The action that will be executed when a message
		/// of type TMessage is sent.</param>
		public void Register<TMessage> (object recipient, object token, Action<TMessage> action)
		{
			// Set up stuff
			var messageType = typeof(TMessage);
			Dictionary<Type, List<WeakActionAndToken>> recipients;

			// Check for recipients dictionary
			if (_Recipients == null)
				_Recipients = new Dictionary<Type, List<WeakActionAndToken>> ();
			recipients = _Recipients;

			List<WeakActionAndToken> list;

			if (!recipients.ContainsKey (messageType)) 
			{
				list = new List<WeakActionAndToken> ();
				recipients.Add (messageType, list);
			} 
			else 
			{
				list = recipients [messageType];
			}

			var weakAction = new WeakAction<TMessage> (recipient, action);
			var item = new WeakActionAndToken 
			{
				Action = weakAction,
				Token = token
			};
			list.Add (item);

			Cleanup ();
		}

		#endregion

		// --- Unregistering

		#region public void Unregister(object recipient)

		/// <summary>
		/// Unregisters a messager recipient completely. After this method
		/// is executed, the recipient will not receive any messages anymore.
		/// </summary>
		/// <param name="recipient">The recipient that must be unregistered.</param>
		public void Unregister (object recipient)
		{
			UnregisterFromLists (recipient, _Recipients);
		}

		#endregion

		#region public void Unregister<TMessage>(object recipient)

		/// <summary>
		/// Unregisters a message recipient for a given type of messages only. 
		/// After this method is executed, the recipient will not receive messages
		/// of type TMessage anymore, but will still receive other message types (if it
		/// registered for them previously).
		/// </summary>
		/// <typeparam name="TMessage">The type of messages that the recipient wants
		/// to unregister from.</typeparam>
		/// <param name="recipient">The recipient that must be unregistered.</param>
		public void Unregister<TMessage> (object recipient)
		{
			Unregister<TMessage> (recipient, null);
		}

		#endregion

		#region public void Unregister<TMessage>(object recipient, Action<TMessage> action)

		/// <summary>
		/// Unregisters a message recipient for a given type of messages and for a given action.
		/// Other message types will still be transmitted to the recipient (if it registered for them previously).
		/// Other actions that have been registered for the message type TMessage and for the given recipient
		/// (if available) will also remain available.
		/// </summary>
		/// <typeparam name="TMessage">The type of messages that the recipient wants to unregister from.</typeparam>
		/// <param name="recipient">The recipient that must be unregistered.</param>
		/// <param name="action">The action that must be unregistered for the recipient and for the message type TMessage.</param>
		public void Unregister<TMessage> (object recipient, Action<TMessage> action)
		{
			UnregisterFromLists (recipient, action, _Recipients);
			Cleanup ();
		}

		#endregion

		// --- Sending

		#region public void Send<TMessage>(TMessage message)

		/// <summary>
		/// Sends a message to registered recipients. The message will
		/// reach all recipients that registered for this message type
		/// using one of the Register methods.
		/// </summary>
		/// <typeparam name="TMessage">The type of message that will be sent.</typeparam>
		/// <param name="message">The message to send to registered recipients.</param>
		public void Send<TMessage> (TMessage message)
		{
			Send(message, null);
		}

		#endregion

		#region public void Send<TMessage>(TMessage message, object token)

		/// <summary>
		/// Sends a message to registered recipients. The message will
		/// reach only recipients that registered for this message type
		/// using one of the Register methods, and that are
		/// of the targetType.
		/// </summary>
		/// <typeparam name="TMessage">The type of message that will be sent.</typeparam>
		/// <param name="message">The message to send to registered recipients.</param>
		/// <param name="token">A token for a messaging channel. If a recipient registers
		/// using a token, and a sender sends a message using the same token, then this
		/// message will be delivered to the recipient. Other recipients who did not
		/// use a token when registering (or who used a different token) will not
		/// get the message. Similarly, messages sent without any token, or with a different
		/// token, will not be delivered to that recipient.</param>
		public void Send<TMessage> (TMessage message, object token)
		{
			// Get the message type
			var messageType = typeof(TMessage);

			// Get the list of recepient objects(WeakActionAndToken list) for message type
			if (_Recipients != null) {
				if (_Recipients.ContainsKey (messageType)) {
					ExecuteActions (message, _Recipients [messageType], token);
				}
			}

			// Do the cleanup after sending
			Cleanup ();
		}

		#endregion

		// --- PUBLIC STATIC METHODS -------------------------------------------------------------------------

		#region private static void CleanupList(IDictionary<Type, List<WeakActionAndToken>> lists)

		/// <summary>
		/// Cleans up the messages -> actions lists for orphaned and null actions
		/// </summary>
		/// <param name="messageLists">Dictionary with message -> actions-lists to cleanup.</param>
		private static void CleanupList (IDictionary<Type, List<WeakActionAndToken>> messageLists)
		{
			// Check
			if (messageLists == null)
				return;

			// REMARK: Could be a bug here. There's something stinky with dictionary handling here
			var listsToRemove = new List<Type> ();
			foreach (var list in messageLists) {
				var recipientsToRemove = new List<WeakActionAndToken> ();
				foreach (var item in list.Value) {
					if (item.Action == null || !item.Action.IsAlive)
						recipientsToRemove.Add (item);
				}

				foreach (var recipient in recipientsToRemove) {
					list.Value.Remove (recipient);
				}

				if (list.Value.Count == 0)
					listsToRemove.Add (list.Key);
			}

			foreach (var key in listsToRemove) {
				messageLists.Remove (key);
			}
		}

		#endregion

		#region private static void ExecuteActions<TMessage>(TMessage message, IEnumerable<WeakActionAndToken> list, object token)

		/// <summary>
		/// Executes actions on a passed list if they are matching send conitions.
		/// </summary>
		/// <typeparam name="TMessage"></typeparam>
		/// <param name="message"></param>
		/// <param name="list"></param>
		/// <param name="token"></param>
		private static void ExecuteActions<TMessage> (TMessage message, IEnumerable<WeakActionAndToken> list, object token)
		{
			if (list != null) {
				// Clone to protect from people registering in a "receive message" method
				var listClone = list.Take (list.Count ()).ToList ();

				// Execute each action in the list if matches conditions
				foreach (var item in listClone) {
					var executeAction = item.Action as IExecuteWithObject;

					if (executeAction != null && item.Action.IsAlive && item.Action.Target != null
					                   && ((item.Token == null && token == null) || item.Token != null && item.Token.Equals (token))) {
						executeAction.ExecuteWithObject (message);
					}
				}
			}
		}

		#endregion

		#region private static void UnregisterFromLists(object recipient, Dictionary<Type, List<WeakActionAndToken>> lists)

		private static void UnregisterFromLists (object recipient, Dictionary<Type, List<WeakActionAndToken>> lists)
		{
			// Check
			if (recipient == null || lists == null || lists.Count == 0)
				return;

			lock (lists) {
				foreach (var messageType in lists.Keys) {
					foreach (var item in lists[messageType]) {
						var weakAction = item.Action;

						if (weakAction != null && recipient == weakAction.Target) {
							weakAction.MarkForDeletion ();
						}
					}
				}
			}
		}

		#endregion

		#region private static void UnregisterFromLists<TMessage>( object recipient, Action<TMessage> action, Dictionary<Type, List<WeakActionAndToken>> lists)

		private static void UnregisterFromLists<TMessage> (object recipient, Action<TMessage> action, Dictionary<Type, List<WeakActionAndToken>> lists)
		{
			// Get the type of the passed message object
			var messageType = typeof(TMessage);

			// Check
			if (recipient == null || lists == null || lists.Count == 0 || !lists.ContainsKey (messageType))
				return;

			lock (lists) 
			{
				foreach (var item in lists[messageType]) {
					var weakActionCasted = item.Action as WeakAction<TMessage>;

					if (weakActionCasted != null && recipient == weakActionCasted.Target && (action == null || action == weakActionCasted.Action)) {
						item.Action.MarkForDeletion ();
					}
				}
			}
		}

		#endregion

		// --- WEAK ACTION AND TOKEN STRUCT -------------------------------------------------------------------------
		private struct WeakActionAndToken
		{
			public WeakAction Action;
			public object Token;
		}
	}
}