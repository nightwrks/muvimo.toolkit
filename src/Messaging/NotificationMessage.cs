﻿
namespace Muvimo.Toolkit.Messaging
{
    /// <summary>
    /// 
    /// Passes a string message (Notification) to a recipient.
    /// <para>Typically, notifications are defined as unique strings in a static class.
    /// To define a unique string, you can use Guid.NewGuid().ToString() or any other unique identifier.</para>
    /// 
    /// </summary>
    public class NotificationMessage : MessageBase
    {

        #region Properties

        /// <summary>
        /// Gets a string containing any arbitrary message to be
        /// passed to recipient(s).
        /// </summary>
        public string Notification
        {
            get;
            private set;
        } 

        #endregion


        /// <summary>
        /// Creates a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="notification">A string containing any arbitrary message to be
        /// passed to recipient(s)</param>
        public NotificationMessage(string notification)
        {
            Notification = notification;
        }

        /// <summary>
        /// Creates a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        /// <param name="notification">A string containing any arbitrary message to be
        /// passed to recipient(s)</param>
        public NotificationMessage(object sender, string notification) : base(sender)
        {
            Notification = notification;
        }

        /// <summary>
        /// Creates a new instance of the NotificationMessage class.
        /// </summary>
        /// <param name="sender">The message's sender.</param>
        /// <param name="target">The message's intended target. This parameter can be used
        /// to give an indication as to whom the message was intended for. Of course
        /// this is only an indication, amd may be null.</param>
        /// <param name="notification">A string containing any arbitrary message to be
        /// passed to recipient(s)</param>
        public NotificationMessage(object sender, object target, string notification) : base(sender, target)
        {
            Notification = notification;
        }

        
    }
}