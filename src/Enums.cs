﻿namespace Muvimo.Toolkit
{
	public enum GradientOrientation
	{
		Horizontal,
		Vertical
	}

	public enum Align : int
	{
		TopLeft = VerticalAlign.Top | HorizontalAlign.Left,
		Top = VerticalAlign.Top | HorizontalAlign.Center,
		TopRight = VerticalAlign.Top | HorizontalAlign.Right,
		MiddleLeft = VerticalAlign.Middle | HorizontalAlign.Left,
		Middle = VerticalAlign.Middle | HorizontalAlign.Center,
		MiddleRight = VerticalAlign.Middle | HorizontalAlign.Right,
		BottomLeft = VerticalAlign.Bottom | HorizontalAlign.Left,
		Bottom = VerticalAlign.Bottom | HorizontalAlign.Center,
		BottomRight = VerticalAlign.Bottom | HorizontalAlign.Right
	};

	public enum VerticalAlign : int
	{
		Top = 4,
		Middle = 16,
		Bottom = 32
	};

	public enum HorizontalAlign : int
	{
		Left = 256,
		Center = 1024,
		Right = 4096
	};

}