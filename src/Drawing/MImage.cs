﻿using System;
using System.IO;
using System.Drawing;

namespace Muvimo.Toolkit.Drawing
{

	public static class MImage
	{
		/// <summary>
		/// Creates the image datta byte[] from passed base64 string 
		/// </summary>
		public static byte[] DataFromBase64String (string base64StringImageData)
		{
			// Safety check
			if (string.IsNullOrEmpty (base64StringImageData))
				return null;

			// Convert base64 string to byte array
			return Convert.FromBase64String (base64StringImageData);
		}


		/// <summary>
		/// Creates the Image object from the passed byte array 
		/// </summary>
		public static Image FromByteArray (byte[] bytes)
		{
			if (bytes == null || bytes.Length == 0)
				return null;

			var stream = new MemoryStream (bytes);
			Image image = Image.FromStream (stream);
            
			stream.Close ();
			stream.Dispose ();

			return image;
		}
	}
}
