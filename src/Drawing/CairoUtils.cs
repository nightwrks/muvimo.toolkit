﻿using System;
using Cairo;

namespace Muvimo.Toolkit.Drawing
{
	public class CairoUtils
	{
		/// <summary>
		/// Creates a Color structure from the four ARGB component (alpha, red, green, and blue) values.
		/// </summary>
		/// <param name="alpha">
		/// The alpha component. Valid values are 0 through 255. 
		/// </param>
		/// <param name="red">
		/// The red component. Valid values are 0 through 255.
		/// </param>
		/// <param name="green">
		/// The green component. Valid values are 0 through 255. 
		/// </param>
		/// <param name="blue">
		/// The blue component. Valid values are 0 through 255. 
		/// </param>
		/// <returns>
		/// A <see cref="Color"/>
		/// </returns>
		public static Color ColorFromArgb( int alpha, int red, int green, int blue )
		{
			return new Color( Math.Round( red / 255.0, 2 ),
				Math.Round( green / 255.0, 2 ),
				Math.Round( blue / 255.0, 2 ),
				Math.Round( alpha / 255.0, 2 ) );
		}

		/// <summary>
		/// Creates a Color structure from the three RGB component (red, green, and blue) values.
		/// </summary>
		/// <param name="red">
		/// The red component. Valid values are 0 through 255.
		/// </param>
		/// <param name="green">
		/// The green component. Valid values are 0 through 255. 
		/// </param>
		/// <param name="blue">
		/// The blue component. Valid values are 0 through 255. 
		/// </param>
		/// <returns>
		/// A <see cref="Color"/>
		/// </returns>
		public static Color ColorFromRgb( int red, int green, int blue )
		{
			return ColorFromArgb( 0, red, green, blue );
		}

		/// <summary>
		/// Ceates a Color structure from the System.Drawing.Color object.
		/// </summary>
		/// <param name="color">
		/// A <see cref="System.Drawing.Color"/>
		/// </param>
		/// <returns>
		/// A <see cref="Color"/>
		/// </returns>
		public static Color ColorFromDrawingColor( System.Drawing.Color color )
		{
			return ColorFromArgb( color.A, color.R, color.G, color.B );
		}

		public static System.Drawing.Color ColorDrawingFromCairoColor( Color color )
		{
			return System.Drawing.Color.FromArgb( Convert.ToInt32( Math.Round( color.A * 255, 0 ) ),
				Convert.ToInt32( Math.Round( color.R * 255, 0 ) ),
				Convert.ToInt32( Math.Round( color.G * 255, 0 ) ),
				Convert.ToInt32( Math.Round( color.B * 255, 0 ) ) );
		}

		public static Color ColorFromGdkColor( Gdk.Color color )
		{
			return ColorFromRgb( color.Red, color.Green, color.Blue );
		}

		public static Gdk.Color ColorGdkFromCairoColor( Color color )
		{
			return new Gdk.Color( Convert.ToByte( Math.Round( color.R * 255, 0 ) ),
				Convert.ToByte( Math.Round( color.G * 255, 0 ) ),
				Convert.ToByte( Math.Round( color.B * 255, 0 ) ) );
		}

		public static Gdk.Color ColorGdkFromDrawingColor( System.Drawing.Color color )
		{
			return new Gdk.Color( color.R, color.G, color.B );
		}

		public static System.Drawing.Color ColorDrawingFromGdkColor( Gdk.Color color )
		{
			return System.Drawing.Color.FromArgb( color.Red, color.Green, color.Blue );
		}

		public static Rectangle RectangleUnion( Rectangle rc1, Rectangle rc2 )
		{
			if( CairoUtils.IsRectangleNull( rc1 ) && CairoUtils.IsRectangleNull( rc2 ) )
				return rc1;
			else
			if( CairoUtils.IsRectangleNull( rc1 ) )
				return rc2;
			else
			if( CairoUtils.IsRectangleNull( rc2 ) )
				return rc1;
                        
			Double x, y, w, h;                 
                        
			if( rc1.X < rc2.X )
				x = rc1.X;
			else
				x = rc2.X;
                        
			if( rc1.Y < rc2.Y )
				y = rc1.Y;
			else
				y = rc2.Y;
                        
			if( ( rc1.Width + rc1.X ) > ( rc2.Width + rc2.X ) )
				w = rc1.Width + rc1.X - x;
			else
				w = rc2.Width + rc2.X - x;
                        
			if( ( rc1.Height + rc1.Y ) > ( rc2.Height + rc2.Y ) )
				h = rc1.Height + rc1.Y - y;
			else
				h = rc2.Height + rc2.Y - y;
                        
			return new Rectangle( x, y, w, h );
		}

		public static bool IsRectangleNull( Rectangle rc )
		{
			if( rc.X == 0 && rc.Y == 0 && rc.Width == 0 && rc.Height == 0 )
				return true;
			else
				return false;
		}
	}
}