﻿using System;

namespace Muvimo.Toolkit.IO
{
	public class Directory
	{

		/// <summary>
		/// Checks if path item is directory.
		/// </summary>
		/// <returns><c>true</c>, if it is directory, <c>false</c> otherwise.</returns>
		/// <param name="path">Path.</param>
		public static bool isDirectory(string path)
		{
			bool result = true;

			// Remove query string if converted from url
			if(path.Contains ("?"))
			{
				path = path.Split ('?') [0];
			}

			System.IO.FileInfo fileTest = new System.IO.FileInfo(path);
			if (fileTest.Exists == true)
			{
				result = false;
			}
			else
			{
				if (fileTest.Extension != "")
				{
					result = false;
				}
			}
			return result;
		}
	}
}

