﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace Muvimo.Framework.Settings
{
    public class SettingsManager<T>
    {
        // Singleton static members
        private static SettingsManager<T> m_SingletonInstance;

        private  T m_Data;
        private string m_Filename = "Settings.xml";
        private string m_FolderPath = "";

        #region Properties
        
        /// <summary>
        /// Gets/Sets the settings data
        /// </summary>
        public T Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }

        /// <summary>
        /// Gets/Sets the path on the device where the settings xml file will be saved.
        /// </summary>
        public string FolderPath
        {
            get { return m_FolderPath; }
            set { m_FolderPath = ValidateFolderPath(value); }
        }

        /// <summary>
        /// Gets/Sets the settings filename. Default is Settings.xml
        /// </summary>
        public string Filename
        {
            get { return m_Filename; }
            set { m_Filename = value; }
        }

        /// <summary>
        /// Gets the fullpath to the settings file ( folderpath + filename )
        /// </summary>
        public string FullPath
        {
            get { return FolderPath + Filename; }
        }

        #endregion


        // --- SINGLETON -----------------------------------------------------------------------------------------------

        #region public static SettingsManager Singleton
        /// <summary>
        /// Gets the SettingsManager's default instance, allowing to register and send messages in a static manner.
        /// </summary>
        public static SettingsManager<T> Singleton
        {
            get
            {
                if ( m_SingletonInstance == null )
                {
                    m_SingletonInstance = new SettingsManager<T>();
                }
                return m_SingletonInstance;
            }
        }
        
        #endregion

        #region public static void Reset()
        /// <summary>
        /// Sets the SettingsManager's default (static) instance to null. 
        /// </summary>
        public static void Reset()
        {
            m_SingletonInstance = null;
        }
        #endregion


        // --- PRIVATE METHODS -------------------------------------------------------------------------------------

        #region private string ValidateFolderPath(string folderPath)
        /// <summary>
        /// Validates if folderpath ends with '/'. If no '/' is added at the end.
        /// </summary>
        /// <param name="folderPath">FolderPath string</param>
        /// <returns>Returns valid folderpath.</returns>
        private string ValidateFolderPath(string folderPath)
        {
            if ( !folderPath.EndsWith(@"/") )
            {
                folderPath += @"/";
            }
            return folderPath;
        } 
        #endregion


        // --- PUBLIC METHODS ---------------------------------------------------------------------------------------

        #region public bool Serialize()
        /// <summary>
        /// Serializes the setting data object as XML settings file.
        /// </summary>
        /// <returns>Returns true is serialization succeded.</returns>
        public bool Serialize()
        {
            // Safety check
            if ( String.IsNullOrEmpty(FolderPath) || FolderPath==@"/" || FolderPath==@"\")
            {
                Debug.WriteLine("SettingsManager.Serialize: Can't serialize, folderpath is invalid.");
                return false;
            }

            if ( String.IsNullOrEmpty(FullPath) )
            {
                Debug.WriteLine("SettingsManager.Serialize: Can't serialize, path is null or empty string.");
                return false;
            }

            bool isSerialized = false;

            // Create an instance of the XmlSerializer class and TextWriter object
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                TextWriter writer = new StreamWriter(FullPath);

                // Serialize the T object, and close the TextWriter.
                serializer.Serialize(writer, m_Data);
                writer.Close();

                isSerialized = true;
            }
            catch ( Exception ex )
            {
                Debug.WriteLine("SettingsManager.Serialize: Exception occured while trying to serialize the setting data. Ex: " + ex);
            }
            return isSerialized;
        } 
        #endregion

        #region public bool Deserialize()
        /// <summary>
        /// Deserializes the settings data.
        /// </summary>
        /// <returns>True if deserialization succeded.</returns>
        public bool Deserialize()
        {
            // Safety check
            if ( String.IsNullOrEmpty(FolderPath) || FolderPath == @"/" || FolderPath == @"\" )
            {
                Debug.WriteLine("SettingsManager.Deserialize: Can't deserialize, folderpath is invalid.");
                return false;
            }

            if ( String.IsNullOrEmpty(FullPath) )
            {
                Debug.WriteLine("SettingsManager.Deserialize: Can't deserialize, path is null or empty string.");
                return false;
            }
            
            // Check if there's settings file
            if ( !File.Exists(FullPath) )
            {
                Debug.WriteLine("SettingsManager.Deserialize: Can't deserialize, there's no settings file.");
                return false;
            }

            bool isSerialized = false;
            try
            {
                // Create an instance of the XmlSerializer class;
                var serializer = new XmlSerializer(typeof(T));

                // If the XML document has been altered with unknown nodes or attributes, handle them with the  UnknownNode and UnknownAttribute events.
                serializer.UnknownNode += Serializer_UnknownNode;
                serializer.UnknownAttribute += Serializer_UnknownAttribute;

                // A FileStream is needed to read the XML document.
                var fs = new FileStream(FullPath, FileMode.Open);

                // Use the Deserialize method to restore the object's state with data from the XML document.
                m_Data = (T)serializer.Deserialize(fs);
                isSerialized = true;
            }
            catch ( Exception ex )
            {
                Debug.WriteLine("SettingsManager.Deserialize: Exception occured while trying to deserialize the setting data. Ex: " + ex);
            }
            return isSerialized;
        } 
        #endregion


        // --- EVENT HANDLING --------------------------------------------------------------------------------------

        #region private void Serializer_UnknownNode (object sender, XmlNodeEventArgs e)
        /// <summary>
        /// Handles when deserializer stop at unknown xml node.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        {
            Debug.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        } 
        #endregion

        #region private void Serializer_UnknownAttribute (object sender, XmlAttributeEventArgs e)
        /// <summary>
        /// Handles when deserializer stop at unknown xml attribute.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {
            System.Xml.XmlAttribute attr = e.Attr;
            Debug.WriteLine("Unknown attribute " + attr.Name + "='" + attr.Value + "'");
        } 
        #endregion

    }
}
