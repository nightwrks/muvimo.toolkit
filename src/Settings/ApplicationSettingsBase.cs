﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Diagnostics;

namespace Muvimo.Toolkit.Settings
{


	public abstract class ApplicationSettingsBase
	{
		// Event handler delegates
		public delegate void AppSettingsEventHander(object o, AppSettingsEventArgs e);

		// EVENTS
		public event AppSettingsEventHander SettingsEvent;


		// PRIVATE MEMBERS
		// ------------------------------------------------------------------------------------
		/// <summary>
		/// Settings location ( path )
		/// </summary>
		private string _Location;


		// PROPERTIES
		// ------------------------------------------------------------------------------------

		/// <summary>
		/// Gets or sets the location ( path ) of the settings.
		/// </summary>
		/// <value>The location.</value>
		public string Location
		{
			get { return _Location; }
			set { _Location = value; }
		}


		//private StringCollection _MyLocations;
		/*
		public StringCollection MyLocations
		{
			get { return _MyLocations; }
			set { _MyLocations = value; }
		}
		*/


		/// <summary>
		/// Initializes a new instance of the <see cref="MStack.ApplicationSettingsBase"/> class.*/
		/// Additional Application specific setting properties can be added here as needed...
		/// </summary>
		public ApplicationSettingsBase()
		{
			InitAppSettings();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MStack.ApplicationSettingsBase"/> class.
		/// </summary>
		/// <param name="path">Path to settings file.</param>
		public ApplicationSettingsBase(string path)
		{
			InitAppSettings();
			this.Load(path);
		}

		/// <summary>
		/// Inits the app settings.
		/// </summary>
		private void InitAppSettings()
		{
			//this._MyLocations = new StringCollection();
		}


		/// <summary>
		/// Saves the app settings.
		/// Returns True if Saved successfully, False otherwise.
		/// Raises Event that includes exception and reason if case of error
		/// </summary>
		/// <returns><c>true</c>, if app settings was saved, <c>false</c> otherwise.</returns>
		/// <param name="path">The path.</param>
		public bool Save(string path)
		{
			try
			{
				XmlSerializer xs = new XmlSerializer(this.GetType());
				FileStream fs = new FileStream(path, FileMode.Create);
				xs.Serialize(fs, this);
				fs.Close();
			}
			catch (FileNotFoundException fex)
			{
				AppSettingsEventArgs e = new AppSettingsEventArgs(SettingsEventReason.FileNotFound, fex);
				SettingsEvent(this, e);
				return false;
			}
			catch (Exception ex)
			{
				AppSettingsEventArgs e = new AppSettingsEventArgs(SettingsEventReason.OtherException, ex);
				SettingsEvent(this, e);
				return false;
			}
			return true;
		}


		/// <summary>
		/// Gets the app settings.
		/// </summary>
		/// <returns>The app settings.</returns>
		/// <param name="path">The path.</param>
		public ApplicationSettingsBase Get(string path)
		{
			// Returns new appSettings object if successfull, null appSettings otherwise
			// Raises Event that includes exception and reason if case of error
			try
			{
				ApplicationSettingsBase appSet;
				XmlSerializer xs = new XmlSerializer(this.GetType());
				FileStream fs = new FileStream(path, FileMode.Open);
				appSet = (ApplicationSettingsBase)xs.Deserialize(fs);
				fs.Close();
				return appSet;
			}

			catch (FileNotFoundException fex)
			{
				AppSettingsEventArgs e = new AppSettingsEventArgs(SettingsEventReason.OtherException, fex);
				SettingsEvent(this, e);
				return (ApplicationSettingsBase)null;
				;
			}
			catch (Exception ex)
			{
				AppSettingsEventArgs e = new AppSettingsEventArgs(SettingsEventReason.OtherException, ex);
				SettingsEvent(this, e);
				return (ApplicationSettingsBase)null;
			}
		}

		/// <summary>
		/// Load the specified thePath.
		/// </summary>
		/// <param name="path">The path.</param>
		public bool Load(string path)
		{
			// This function loads the settings file directly to "this" instance of appSettings
			// instead of returning the appSettings Object
			// Returns true on success, false otherwise
			// Raises Event that includes exception and reason if case of error
			try
			{
				ApplicationSettingsBase appSet;
				XmlSerializer xs = new XmlSerializer(this.GetType());
				FileStream fs = new FileStream(path, FileMode.Open);
				appSet = (ApplicationSettingsBase)xs.Deserialize(fs);
				foreach (System.Reflection.PropertyInfo pi in this.GetType().GetProperties())
				{
					if (pi.CanWrite)
					{
						try
						{
							pi.SetValue(this, pi.GetValue(appSet,null),null);
						}
						catch (Exception ex)
						{
							pi.SetValue(this, null, null);
						}
					}
				}
				fs.Close();
			}

			catch (FileNotFoundException fex)
			{
				AppSettingsEventArgs e = new AppSettingsEventArgs(SettingsEventReason.OtherException, fex);
				SettingsEvent(this, e);
				return false;
				;
			}
			catch (Exception ex)
			{
				AppSettingsEventArgs e = new AppSettingsEventArgs(SettingsEventReason.OtherException, ex);
				SettingsEvent(this, e);
				return false;
			}
			return true;
		}

	}  
	//  </class ApplicationSettingsBase>




	public class AppSettingsEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MStack.AppSettingsEventArgs"/> class.
		/// </summary>
		/// <param name="reason">Reason.</param>
		/// <param name="objInfo">Object info.</param>
		public AppSettingsEventArgs(SettingsEventReason reason, object objInfo)
		{
			this._settingsEvent = reason;
			this._ObjInfo = objInfo;
		}


		private object _ObjInfo;
		private SettingsEventReason _settingsEvent;

		// Public Property(s) to return value(s)
		public object objInfo
		{
			get { return _ObjInfo; }
		}

		public SettingsEventReason EventReason
		{
			get { return (SettingsEventReason)_settingsEvent; }
		}
	}   
	// </ ChangeEventArgs> 



	public enum SettingsEventReason
	{

		FileNotFound = 1,
		// Add additional event reasons to enum here
		OtherException = 99
	}
}

