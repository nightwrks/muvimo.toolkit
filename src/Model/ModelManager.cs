﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Muvimo.Toolkit.Model
{
    public class ModelManager
    {
        // Members
        private static ModelManager m_SingletonInstance;
        private Dictionary<string, ModelBase> m_Models;

        #region Properties

        /// <summary>
        /// Gets the dictionary of key-model name, value-ModelBase object
        /// </summary>
        public Dictionary<string, ModelBase> Models
        {
            get { return m_Models; }
        } 

        #endregion

        // --- SINGLETON ---------------------------------------------------------------------------------

        #region public static ModelManager Singleton
        /// <summary>
        /// Gets the ModelManager's default instance, allowing to register and send messages in a static manner.
        /// </summary>
        public static ModelManager Singleton
        {
            get
            {
                if ( m_SingletonInstance == null )
                {
                    m_SingletonInstance = new ModelManager();
                }
                return m_SingletonInstance;
            }
        }

        #endregion

        #region public static void Reset()
        /// <summary>
        /// Sets the ModelManager's default (static) instance to null.
        /// Disposes all Models and clears the Models dict.
        /// </summary>
        public static void Reset()
        {
            Singleton.Clear(true);
            m_SingletonInstance = null;
        }
        #endregion


        // --- PUBLIC ---------------------------------------------------------------------------------------

        #region public void Register(string name, ModelBase model)
        /// <summary>
        /// Registers the model object with ModelManager
        /// </summary>
        /// <param name="name"></param>
        /// <param name="model"></param>
        public void Register(string name, ModelBase model)
        {
            if ( String.IsNullOrEmpty(name) )
            {
                Debug.WriteLine("ModelManager.Register: name is empty or null string. Not registering the model");
                return;
            }
            if ( model == null )
            {
                Debug.WriteLine("ModelManager.Register: model is null object. Not registering the model");
                return;
            }

            // Create new Models dict if not initialzied already
            if ( m_Models == null ) m_Models = new Dictionary<string, ModelBase>();

            if ( !Models.ContainsKey(name) ) Models.Add(name, model);
        } 
        #endregion

        #region public void Unregister(string name)
        /// <summary>
        /// Unregisters the model from ModelManager
        /// </summary>
        /// <param name="name"></param>
        public void Unregister(string name)
        {
            if ( String.IsNullOrEmpty(name) )
            {
                Debug.WriteLine("ModelManager.Unregister: name is empty or null string. Not unregistering the model");
                return;
            }

            if ( !Models.ContainsKey(name) ) Models.Remove(name);
        } 
        #endregion

        #region public void Clear(bool disposeModels)
        /// <summary>
        /// Clears the Models dictionary and disposes all Models if asked.
        /// </summary>
        /// <param name="disposeModels">True to dispose all Model object in the dictionary.</param>
        public void Clear(bool disposeModels)
        {
            // Check
            if ( m_Models == null )
            {
                Debug.WriteLine("ModelManager.Clear: Modles dictionary is null object.");
                return;
            }

            // Dispose all models is told so
            if ( disposeModels )
            {
                foreach ( ModelBase model in Models.Values )
                {
                    model.Dispose();
                }
            }
            m_Models.Clear();
        } 
        #endregion



    }
}
