﻿using System;

namespace Muvimo.Toolkit.Model
{
    public class ModelBase : IDisposable
    {
        // Consts
        public const string NAME = "ModelBase";
        

        // --- CTOR / DISPOSAL ---------------------------------------------------------------------------------
        public  ModelBase()
        {
        }

        #region public void Dispose()
        /// <summary>
        /// Disposes the Model object
        /// </summary>
        public void Dispose()
        {
            OnDispose();
        } 
        #endregion


        // --- PROTECTED VIRTUAL METHODS -----------------------------------------------------------------

        #region protected virtual void OnDispose()
        protected virtual void OnDispose()
        {
        } 
        #endregion


    }
}
