﻿using System;
using System.IO;
using ServiceStack.Text;
using System.Diagnostics;

namespace Muvimo.Toolkit.Serialization
{
	public class Json
	{

		/// <summary>
		/// Deserializes the object from json file.
		/// </summary>
		/// <returns>T object created from json.</returns>
		/// <param name="path">Path to json file.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T FromFile<T>( string path )
		{
			T obj = default(T);
			if (File.Exists( path )) 
			{
				var jsonString = File.ReadAllText (path);
				obj = JsonSerializer.DeserializeFromString<T> (jsonString);
			}
			else
			{
				Debug.WriteLine ("Json file doesn't exists. Path: {0}", path );
			}
			return obj;
		}


		/// <summary>
		/// Serializes the passed object to json and saves it to specified disk path
		/// </summary>
		/// <param name="toSerialize">Object to serialize.</param>
		/// <param name="path">Path where to save the json.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void ToFile<T>( T toSerialize, string path )
		{
			try
			{
				if(File.Exists (path)) 
					File.Delete (path);

				var jsonString = JsonSerializer.SerializeToString<T>( toSerialize );
				File.WriteAllText ( path, jsonString );
			}
			catch(Exception ex)
			{
				Debug.WriteLine ("Muvimo.Toolkit.Serialization.Json::ToFile: Exception while deserializing. {0}", path );
				Debug.WriteLine ("Exception: {0}", ex.ToString () );
			}
		}
	}
}

