﻿using System;
using Cairo;
using Muvimo.Toolkit.Drawing;
using Gtk;

namespace Muvimo.Toolkit.UI.GtkSharp
{
	[System.ComponentModel.ToolboxItem( true )]
	public class GradientButton : ButtonBase
	{
		// MEMBERS

		// Background colors
		private Cairo.Color _BgStartColor = CairoUtils.ColorFromDrawingColor( System.Drawing.Color.AliceBlue );
		private Cairo.Color _BgEndColor = CairoUtils.ColorFromDrawingColor( System.Drawing.Color.LightSteelBlue );
		private Cairo.Color _MouseOverBgStartColor = CairoUtils.ColorFromDrawingColor( System.Drawing.Color.Beige );
		private Cairo.Color _MouseOverBgEndColor = CairoUtils.ColorFromDrawingColor( System.Drawing.Color.Beige );
		private Cairo.Color _MouseDownBgStartColor = CairoUtils.ColorFromDrawingColor( System.Drawing.Color.Bisque );
		private Cairo.Color _MouseDownBgEndColor = CairoUtils.ColorFromDrawingColor( System.Drawing.Color.Bisque );


		// Border
		private Cairo.Color _BorderColor =	CairoUtils.ColorFromDrawingColor( System.Drawing.Color.Black );
		private int _BorderThickness = 2;

		private GradientOrientation _GradientOrientation = GradientOrientation.Vertical;
		private bool _GradientMirror = false;
		private int _CornerRadius = 0;



		#region PROPERTIES

		/// <summary>
		/// Gets or sets the start color.
		/// </summary>
		/// <value>The start color.</value>
		public System.Drawing.Color StartColor 
		{
			get { return CairoUtils.ColorDrawingFromCairoColor( _BgStartColor ); }
			set
			{                               
				_BgStartColor = CairoUtils.ColorFromDrawingColor( value );
				QueueDraw();
			}                       
		}

		/// <summary>
		/// Gets or sets the end color.
		/// </summary>
		/// <value>The end color.</value>
		public System.Drawing.Color EndColor 
		{
			get { return CairoUtils.ColorDrawingFromCairoColor( _BgEndColor ); }
			set
			{
				_BgEndColor = CairoUtils.ColorFromDrawingColor( value );
				QueueDraw();
			}                       
		}

		/// <summary>
		/// Gets or sets the gradient orientation.
		/// </summary>
		/// <value>The gradient orientation.</value>
		public GradientOrientation GradientOrientation 
		{
			get { return _GradientOrientation; }
			set
			{
				if( _GradientOrientation != value )
				{
					_GradientOrientation = value;
					QueueDraw();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="Muvimo.Toolkit.UI.GtkSharp.GradientButton"/> gradient mirror.
		/// </summary>
		/// <value><c>true</c> if gradient mirror; otherwise, <c>false</c>.</value>
		public bool GradientMirror 
		{
			get { return _GradientMirror; }
			set
			{
				if( _GradientMirror != value )
				{
					_GradientMirror = value;
					QueueDraw();
				}
			}
		}

		/// <summary>
		/// Gets or sets the corner radius.
		/// </summary>
		/// <value>The corner radius.</value>
		public int CornerRadius 
		{
			get { return _CornerRadius; }
			set
			{
				if( _CornerRadius != value )
				{
					_CornerRadius = value;
					QueueDraw();
				}
			}
		}

		/// <summary>
		/// Gets or sets the color of the mouse over.
		/// </summary>
		/// <value>The color of the mouse over.</value>
		public System.Drawing.Color MouseOverColor 
		{
			get { return CairoUtils.ColorDrawingFromCairoColor( _MouseOverBgStartColor ); }
			set
			{
				_MouseOverBgStartColor = CairoUtils.ColorFromDrawingColor( value );
			}                       
		}

		/// <summary>
		/// Gets or sets the color of the mouse down.
		/// </summary>
		/// <value>The color of the mouse down.</value>
		public System.Drawing.Color MouseDownColor 
		{
			get{ return CairoUtils.ColorDrawingFromCairoColor( _MouseDownBgStartColor ); }
			set
			{ 
				_MouseDownBgStartColor = CairoUtils.ColorFromDrawingColor( value );
			}                       
		}

		/// <summary>
		/// Gets or sets the color of the border.
		/// </summary>
		/// <value>The color of the border.</value>
		public System.Drawing.Color BorderColor 
		{
			get { return CairoUtils.ColorDrawingFromCairoColor( _BorderColor ); }
			set
			{ 
				_BorderColor = CairoUtils.ColorFromDrawingColor( value );
				QueueDraw();
			}                       
		}

		/// <summary>
		/// Gets or sets the border tickness.
		/// </summary>
		/// <value>The border tickness.</value>
		public int BorderTickness 
		{
			get { return _BorderThickness; }
			set
			{
				_BorderThickness = value;
				QueueDraw();
			}
		}

		#endregion


		// CTOR / DISPOSAL
		// -------------------------------------------------------------------------------------------------------

		public GradientButton() : base() {}

		public GradientButton ( string text ) : base(text) {}


		// OVERRIDES
		// -------------------------------------------------------------------------------------------------------

		/// <summary>
		/// Draws the background.
		/// </summary>
		/// <param name="cr">Cr.</param>
		/// <param name="ev">Ev.</param>
		protected override void DrawBackground( Cairo.Context cr, Gdk.EventExpose ev )
		{
			Double tick = _BorderThickness / 2.0;
                        
			cr.Rectangle( ev.Area.X, ev.Area.Y, ev.Area.Width, ev.Area.Height );
			cr.Clip();

			// Draw outline
			cr.NewPath();
			if( _CornerRadius > 0 )
			{
				cr.MoveTo( tick, tick + _CornerRadius );
				cr.CurveTo( tick, tick + _CornerRadius / 2.0, tick + _CornerRadius / 2.0, tick,	tick + _CornerRadius, tick );
				cr.LineTo( Allocation.Width - tick - _CornerRadius, tick );
				cr.CurveTo( Allocation.Width - tick - _CornerRadius / 2.0, tick,	Allocation.Width - tick, tick + _CornerRadius / 2.0,	Allocation.Width - tick, tick + _CornerRadius );
				cr.LineTo( Allocation.Width - tick, Allocation.Height - tick - _CornerRadius );
				cr.CurveTo( Allocation.Width - tick, Allocation.Height - tick - _CornerRadius / 2.0,	Allocation.Width - tick - _CornerRadius / 2.0, Allocation.Height - tick,	Allocation.Width - tick - _CornerRadius, Allocation.Height - tick );
				cr.LineTo( tick + _CornerRadius, Allocation.Height - tick );
				cr.CurveTo( tick + _CornerRadius / 2.0, Allocation.Height - tick, tick, Allocation.Height - tick - _CornerRadius / 2.0,	tick, Allocation.Height - tick - _CornerRadius );
				cr.LineTo( tick, tick + _CornerRadius );
			}
			else
			{
				cr.MoveTo( tick, tick );
				cr.LineTo( Allocation.Width - tick, tick );
				cr.LineTo( Allocation.Width - tick, Allocation.Height - tick );
				cr.LineTo( tick, Allocation.Height - tick );
				cr.LineTo( tick, tick + _CornerRadius );
			}
			cr.ClosePath();

                        
			if( !IsMouseDown )
			{
				if( !IsMouseOver )
				{
					if( !_BgStartColor.Equals(_BgEndColor))
					{
						Cairo.LinearGradient linGrad;
						if( _GradientOrientation == GradientOrientation.Vertical )
						{
							linGrad = new Cairo.LinearGradient( 0, 0, 0, Allocation.Height );
						}
						else
						{
							linGrad = new Cairo.LinearGradient( 0, 0, Allocation.Width, 0 );
						}

						if( _GradientMirror )
						{
							linGrad.AddColorStop( 0, _BgStartColor );
							linGrad.AddColorStop( 0.5, _BgEndColor );
							linGrad.AddColorStop( 1, _BgStartColor );
						}
						else
						{
							linGrad.AddColorStop( 0, _BgStartColor );
							linGrad.AddColorStop( 1, _BgEndColor );
						}                                        
						cr.Pattern = linGrad;
					}
					else
					{
						cr.Color = _BgStartColor;
					}

				}
				else
				{
					cr.Color = _MouseOverBgStartColor;
				}
			}
			else
			{
				cr.Color = _MouseDownBgStartColor;
			}
			cr.FillPreserve();
		}

		/// <summary>
		/// Draws the border.
		/// </summary>
		/// <param name="cr">Cr.</param>
		/// <param name="ev">Ev.</param>
		protected override void DrawBorder( Cairo.Context cr, Gdk.EventExpose ev )
		{
			cr.LineWidth = _BorderThickness;
			cr.Color = _BorderColor;
			cr.Stroke();
		}

		/// <summary>
		/// Draws the image.
		/// </summary>
		/// <param name="cr">Cr.</param>
		/// <param name="ev">Ev.</param>
		protected override void DrawImage( Cairo.Context cr, Gdk.EventExpose ev )
		{
			if( Icon != null )
			{                               
				Point pt = GetImagePosition();
				ev.Window.DrawPixbuf( this.Style.ForegroundGC( Gtk.StateType.Normal ), Icon, 0, 0, pt.X, pt.Y, Icon.Width, Icon.Height, Gdk.RgbDither.Normal, 0, 0 );
			}
		}

		/// <summary>
		/// Draws the text.
		/// </summary>
		/// <param name="cr">Cr.</param>
		/// <param name="ev">Ev.</param>
		protected override void DrawText( Cairo.Context cr, Gdk.EventExpose ev )
		{
			Gdk.PangoRenderer renderer = Gdk.PangoRenderer.GetDefault( ev.Window.Screen );
			renderer.Drawable = ev.Window;
			renderer.Gc = this.Style.TextGC( Gtk.StateType.Normal );
			renderer.SetOverrideColor( Pango.RenderPart.Foreground, TextColor );
			PangoLayout.Width = Pango.Units.FromPixels( this.Allocation.Width );           
			Point pt = GetTextPosition();
			renderer.DrawLayout( PangoLayout, Pango.Units.FromPixels( pt.X ), Pango.Units.FromPixels( pt.Y ) );
			renderer.SetOverrideColor( Pango.RenderPart.Foreground, Gdk.Color.Zero );
			renderer.Drawable = null;
			renderer.Gc = null;
			//ev.Window.DrawLayout(this.Style.TextGC (Gtk.StateType.Normal),pt.X,pt.Y,layout);
		}

		/// <summary>
		/// Calculates text position, which depends on TextAllign
		/// </summary>
		/// <returns>
		/// Uper left point of text.
		/// A <see cref="Point"/>
		/// </returns>
		protected override Point GetTextPosition()
		{
			int h, w;
			PangoLayout.GetPixelSize( out w, out h );
			if( TextVerticalAlign == VerticalAlign.Bottom )
			{
				h = this.Allocation.Height - _BorderThickness - h;
			}				
			else if( TextVerticalAlign == VerticalAlign.Middle )
			{
				h = this.Allocation.Height / 2 - h / 2;
			}				
			else
			{
				h = _BorderThickness;                     
			}				
			return new Point( 0, h );
		}

		/// <summary>
		/// Calculates area where text is drawn
		/// </summary>
		/// <returns>
		/// A <see cref="Rectangle"/>
		/// </returns>
		protected override Rectangle GetTextArea()
		{
			int h, w;
			int y;
			Pango.Rectangle inkRect;
			Pango.Rectangle logRect;
			PangoLayout.GetPixelExtents( out inkRect, out logRect );
			PangoLayout.GetPixelSize( out w, out h );

			if( TextVerticalAlign == VerticalAlign.Bottom )
			{
				y = this.Allocation.Height - _BorderThickness - h;
			}				
			else if( TextVerticalAlign == VerticalAlign.Middle )
			{
				y = this.Allocation.Height / 2 - h / 2;
			}				
			else
			{
				y = _BorderThickness;
			}				
			return new Rectangle( logRect.X, y, w, h );
		}

		/// <summary>
		/// Gets the image position.
		/// </summary>
		/// <returns>The image position.</returns>
		protected override Point GetImagePosition()
		{
			int x = 0;
			int y = 0;
			int imageOffset = 8;
                        
			if( Icon != null )
			{                       
				if( (VerticalAlign)( (int)IconAlign & (int)VerticalAlign.Bottom ) == VerticalAlign.Bottom )
				{
					y = this.Allocation.Height - _BorderThickness - imageOffset - Icon.Height;
				}					
				else if( (VerticalAlign)( (int)IconAlign & (int)VerticalAlign.Middle ) == VerticalAlign.Middle )
				{
					y = this.Allocation.Height / 2 - this.Icon.Height / 2;
				}						
				else
				{
					y = _BorderThickness + imageOffset;
				}
					
                                        
				if( (HorizontalAlign)( (int)IconAlign & (int)HorizontalAlign.Center ) == HorizontalAlign.Center )
				{
					x = this.Allocation.Width / 2 - this.Icon.Width / 2;
				}					
				else if( (HorizontalAlign)( (int)IconAlign & (int)HorizontalAlign.Right ) == HorizontalAlign.Right )
				{
					if( _CornerRadius < 5 || (VerticalAlign)( (int)IconAlign & (int)VerticalAlign.Middle ) == VerticalAlign.Middle )
					{
						x = this.Allocation.Width - _BorderThickness - imageOffset - this.Icon.Width;
					}							
					else
					{
						x = this.Allocation.Width - _BorderThickness - imageOffset - this.Icon.Width - ( _CornerRadius / 3 ) * 2;
					}
				}
				else
				{
					if( _CornerRadius < 5 || (VerticalAlign)( (int)IconAlign & (int)VerticalAlign.Middle ) == VerticalAlign.Middle )
					{
						x = _BorderThickness + imageOffset;
					}
					else
					{
						x = ( _CornerRadius / 3 ) * 2;
					}
				}
			}                        
			return new Point( x, y );
		}






	}
}