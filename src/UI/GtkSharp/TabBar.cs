﻿using System;
using Gtk;


namespace Muvimo.Toolkit.UI.GtkSharp
{
	public class TabBar: HBox
	{
		// Events and handlers
		public delegate void ActiveTabChangedHandler(object sender, TabEventArgs e);
		public event ActiveTabChangedHandler ActiveTabChanged;

		public delegate void TabAddedHandler(object sender, TabEventArgs e);
		public event TabAddedHandler TabAdded;

		public delegate void TabRemovedHandler(object sender, TabRemovedEventArgs e);
		public event TabRemovedHandler TabRemoved;


		// MEMBERS
		private bool tabsShouldBeFitted;
		private uint _tabPadding { get; set; }


		#region Properties

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="Muvimo.Kamino.UI.WIdgets.TabHBox"/> will be able to remove child tab with middle mouse click.
		/// </summary>
		/// <value><c>true</c> if remove tab with middle mouse; otherwise, <c>false</c>.</value>
		public bool RemoveTabWithMiddleMouse { get; set; }

		/// <summary>
		/// Gets or sets the page container.
		/// </summary>
		/// <value>The page container.</value>
		public Box PageContainer { get; protected set; }

		/// <summary>
		/// Gets current active tab.
		/// </summary>
		/// <value>The current active tab.</value>
		public Gtk.Widget CurrentActiveTab { get; protected set; }

		/// <summary>
		/// Gets current active tab page.
		/// </summary>
		/// <value>The current active page.</value>
		public Gtk.Widget CurrentActivePage { get; protected set; }

		/// <summary>
		/// Gets or sets the maximal width of the tab item.
		/// </summary>
		/// <value>The width of the tab max.</value>
		public int TabMaxWidth { get; set; }

		#endregion


		// CTOR / DISPOSAL
		// ---------------------------------------------------------------------------------------------------
		public TabBar( Box pageContainer )
		{
			TabMaxWidth = 150;
			_tabPadding = 0;

			if( pageContainer == null )
				throw new NullReferenceException( "PageContainer is null. Cannot create new instance." );

			PageContainer = pageContainer;
			PageContainer.NoShowAll = true;
			PageContainer.Show();
		}

		protected override void OnDestroyed()
		{
			RemoveAll();
			base.OnDestroyed();
		}


		//
		// PUBLIC
		// ---------------------------------------------------------------------------------------------------

		/// <summary>
		/// Appends the tab item and tab page.
		/// </summary>
		/// <param name="tabItem">Tab item.</param>
		/// <param name="tabPage">Tab page.</param>
		/// <param name="makeActive">If set to <c>true</c> make active.</param>
		public void AppendItem( Gtk.Widget tabWidget, Gtk.Widget tabPage, bool makeActive = true )
		{
			// Checks
			if( tabWidget == null ) return;
			if( tabPage == null ) return;

			// Wrap tabItems into event boxes to be able to catch button and other events
			EventBox tabEventBox = new EventBox();
			tabEventBox.Name = tabWidget.Name;
			tabEventBox.ButtonReleaseEvent += HandleTabItemButtonReleaseEvent;
			tabEventBox.EnterNotifyEvent += HandleTabItemEnterNotifyEvent;

			// Add new tab
			tabEventBox.Add( tabWidget );
			//tabEventBox.WidthRequest = TabMaxWidth;
			PackStart( tabEventBox, false, false, _tabPadding );

			// Add new page
			PageContainer.PackStart( tabPage, true, true, 0 );

			// Activate tab if told so
			if( makeActive )
			{
				ActivateTab( tabEventBox );
			}

			// Fire TabAdded event
			if (TabAdded != null) 
			{
				TabEventArgs args = new TabEventArgs( tabEventBox, tabWidget, tabPage );
				TabAdded( this, args );
			}
		}


		/// <summary>
		/// Removes and disposes all items and pages.
		/// </summary>
		public void RemoveAll()
		{
			foreach( var item in Children )
			{
				Remove( item );
			}
		}

		/// <summary>
		/// Activates the tab at index.
		/// </summary>
		/// <param name="index">Index.</param>
		public void ActivateTabAt( int index )
		{
			OnActivateTab( (Gtk.EventBox)Children[index], null );
		}


		/// <summary>
		/// Activates the tab.
		/// </summary>
		/// <param name="tabItem">Tab item.</param>
		public void ActivateTab( Gtk.EventBox tabItem )
		{
			OnActivateTab( tabItem, null );
		}



		//
		// PRIVATE
		// ---------------------------------------------------------------------------------------------------

		/// <summary>
		/// Gets the linked page for specified tab item.
		/// </summary>
		/// <returns>The linked page.</returns>
		/// <param name="tabItem">Tab item.</param>
		private Gtk.Widget GetLinkedPage( Gtk.EventBox tabItem )
		{
			// Checks
			if( tabItem == null ) return null;
			if( PageContainer == null ) return null;

			// Get Linked page ( same index as tab item )
			Gtk.Widget page = null;
			if( this.Children != null )
			{
				int index = Array.IndexOf( this.Children, tabItem );
				if( index >= 0 )
					page = PageContainer.Children[ index ];
			}
			return page;
		}


		/// <summary>
		/// Removes and disposes the page for specified tab item.
		/// </summary>
		/// <param name="tabItem">Tab item.</param>
		private void RemovePage( Gtk.EventBox tabItem )
		{
			// Checks
			if( tabItem == null ) return;
			if( PageContainer == null ) return;

			// Remove the page
			var pageItem = GetLinkedPage( tabItem );
			if( pageItem != null )
			{
				PageContainer.Remove( pageItem );
				pageItem.Destroy();
				if( pageItem != null ) pageItem.Dispose();
			}
		}


		/// <summary>
		/// Removes and disposes the page at index.
		/// </summary>
		/// <param name="index">Index.</param>
		private void RemovePageAt( int index )
		{
			// Check
			if( PageContainer == null ) return;

			// Remove the page widget from it's container
			var pageItem = PageContainer.Children[ index ];
			if( pageItem != null )
			{
				PageContainer.Remove(pageItem);
				pageItem.Destroy();
				if( pageItem != null ) pageItem.Dispose();
			}
		}



		//
		// EVENT HANDLERS
		// ---------------------------------------------------------------------------------------------------

		private void HandleTabItemButtonReleaseEvent( object o, ButtonReleaseEventArgs args )
		{
			switch( args.Event.Button )
			{
				case 1:

					// LEFT MOUSE BUTTON. Select tab
					OnActivateTab( (Gtk.EventBox)o, args );
					break;

					// MIDDLE MOUSE BUTTON. Close tab
				case 2:
					OnTabMiddleMouseRelease((Gtk.EventBox)o, args );
					break;

					// RIGHT MOUSE BUTTON. Maybe menu?
				case 3:
					OnTabRightMouseRelease( (Gtk.EventBox)o, args );
					break;

				default:
					break;
			}
		}

		private void HandleTabItemEnterNotifyEvent( object o, EnterNotifyEventArgs args )
		{
			OnTabItemMotionNotify( (Gtk.EventBox)o, args );
		}


		//
		// PROTECTED
		// ---------------------------------------------------------------------------------------------------

		protected virtual void OnTabLeftMouseRelease( Gtk.EventBox tabItem, ButtonReleaseEventArgs args )
		{
			OnActivateTab( tabItem, args );
		}

		protected virtual void OnTabMiddleMouseRelease( Gtk.EventBox tabItem, ButtonReleaseEventArgs args )
		{
			if( RemoveTabWithMiddleMouse )
			{
				if( Children.Length == 1 ) return;
				Remove( tabItem );
			}
		}

		protected virtual void OnTabRightMouseRelease( Gtk.EventBox tabItem, ButtonReleaseEventArgs args )
		{
		}

		protected override void OnSizeAllocated(Gdk.Rectangle allocation)
		{
			base.OnSizeAllocated(allocation);

			var itemsMaxWidth = 0;
			foreach( var tabItem in Children )
			{
				itemsMaxWidth += (TabMaxWidth + (int)(_tabPadding*2));
			}

			Console.WriteLine("Max width: " + itemsMaxWidth );
			Console.WriteLine("Alloc width: " + Allocation.Width );

			if( itemsMaxWidth < Allocation.Width )
			{
				// Items in max width are smaller that current parent width
				// In that cas they shouldn't be fitted
				if( tabsShouldBeFitted == false ) return; 
				foreach( Gtk.EventBox tabItem in Children )
				{
					this.SetChildPacking( tabItem, false, true, _tabPadding, PackType.Start );
				}
				tabsShouldBeFitted = false;
			}
			else
			{
				// Items in max width are larger that current parent width
				// In that case they should be fitted to stretch with  parent
				if( tabsShouldBeFitted == true ) return; 
				foreach( Gtk.EventBox tabItem in Children )
				{
					this.SetChildPacking( tabItem, true, true, _tabPadding, PackType.Start );
				}
				tabsShouldBeFitted = true;
			}
		}


		/// <summary>
		/// Activates the tab and raises the activate tab event.
		/// </summary>
		/// <param name="tabItem">Tab item.</param>
		/// <param name="args">Arguments.</param>
		protected virtual void OnActivateTab( Gtk.EventBox tabItem, ButtonReleaseEventArgs args )
		{
			// Checks
			if( tabItem == null ) return;

			// Do the [de]activation
			if( CurrentActiveTab != null )
			{
				CurrentActiveTab.State = StateType.Normal;
			}

			CurrentActiveTab = tabItem;
			CurrentActiveTab.Activate();

			// Hide all tab pages
			foreach( var page in PageContainer.Children )
			{
				page.HideAll();
			}
			CurrentActivePage = GetLinkedPage( tabItem );
			CurrentActivePage.ShowAll();

			// Fire ActiveTabChanged event
			if( ActiveTabChanged != null ) 
			{
				TabEventArgs tabArgs = new TabEventArgs( tabItem, tabItem.Child, CurrentActivePage );
				ActiveTabChanged( this, tabArgs);
			}
		}

		/// <summary>
		/// Raises the tab item motion notify event.
		/// </summary>
		/// <param name="tabItem">Tab item.</param>
		/// <param name="args">Arguments.</param>
		protected virtual void OnTabItemMotionNotify( Gtk.EventBox tabItem, EnterNotifyEventArgs args )
		{
		}


		//
		// OVERRIDES
		// ---------------------------------------------------------------------------------------------------

		/// <summary>
		/// Overriden OnRemoved
		/// </summary>
		/// <param name="widget">Widget.</param>
		protected override void OnRemoved( Gtk.Widget widget )
		{
			// Check
			if( widget == null ) return;

			string name = widget.Name;
			int index = Array.IndexOf( Children, widget );

			// Do the corresponding page removal
			RemovePage( (Gtk.EventBox)widget );

			// Unsubscribe from events
			widget.ButtonReleaseEvent -= HandleTabItemButtonReleaseEvent;
			widget.EnterNotifyEvent -= HandleTabItemEnterNotifyEvent;

			base.OnRemoved( widget );

			/* We don't need this since base is removing the widget
			if( widget != null ) widget.Destroy();
			if( widget != null ) widget.Dispose();
			*/

			// Fire TabRemoved event
			if (TabRemoved != null) 
			{
				TabRemovedEventArgs args = new TabRemovedEventArgs( name, "" );
				TabRemoved( this, args );
			}

			// Activate tab before removed one
			if( index > 0 )
				ActivateTabAt( index - 1 );
		}

	}




	/// <summary>
	/// Tab event arguments.
	/// </summary>
	public class TabEventArgs : EventArgs
	{
		public Gtk.EventBox TabItemWrapper { get; private set; }
		public Gtk.Widget TabItem { get; private set; }
		public Gtk.Widget TabPage { get; private set; }

		public TabEventArgs( Gtk.EventBox tabItemWrapper, Gtk.Widget tabItem, Gtk.Widget tabPage )
		{
			TabItemWrapper = tabItemWrapper;
			TabItem = tabItem;
			TabPage = tabPage;
		}
	}


	/// <summary>
	/// Tab removed event arguments.
	/// </summary>
	public class TabRemovedEventArgs : EventArgs
	{
		public string TabName { get; private set; }
		public string Tag { get; private set; }

		public TabRemovedEventArgs( string tabName, string tag )
		{
			this.TabName = tabName;
			this.Tag = tag;
		}
	}


}

