﻿using Cairo;
using System;
using Muvimo.Toolkit.Drawing;
using System.Diagnostics;

namespace Muvimo.Toolkit.UI.GtkSharp
{
	[System.ComponentModel.ToolboxItem( true )]
	public class ButtonBase : Gtk.DrawingArea
	{
		// EVENT DELEGATES
		public delegate void PressedEventHandler( object sender, EventArgs e );
		public delegate void ReleasedEventHandler( object sender, EventArgs e );

		// EVENTS
		﻿public event PressedEventHandler Pressed;

//		﻿private PressedEventHandler PressedEvent;		﻿  ﻿
//		﻿public event PressedEventHandler Pressed
//		{
//﻿  ﻿  ﻿  add { PressedEvent = (PressedEventHandler)System.Delegate.Combine( PressedEvent, value );	}
//﻿  ﻿  ﻿  remove { PressedEvent = (PressedEventHandler)System.Delegate.Remove( PressedEvent, value );	}
//		}

		public event ReleasedEventHandler Released;

//﻿  ﻿  private ReleasedEventHandler ReleasedEvent;
//﻿  ﻿  public event ReleasedEventHandler Released
//		{
//﻿  ﻿  ﻿  add { ReleasedEvent = (ReleasedEventHandler)System.Delegate.Combine( ReleasedEvent, value ); }
//﻿  ﻿  ﻿  remove { ReleasedEvent = (ReleasedEventHandler)System.Delegate.Remove( ReleasedEvent, value ); }
//		}



		// MEMBERS - PROTECTED
		protected bool IsMouseDown = false;
﻿  ﻿  protected bool IsMouseOver = false;
﻿  ﻿  protected Pango.Layout PangoLayout;
		protected VerticalAlign TextVerticalAlign = VerticalAlign.Middle;

		// MEMBERS - PRIVATE

		// Text
		private Gdk.Color _TextColor = CairoUtils.ColorGdkFromDrawingColor( System.Drawing.Color.Black );

		// Icon
		private Gdk.Pixbuf _Icon;
		private Align _IconAlign = Align.MiddleLeft;

		// Misc
		public int MinWidth { get; protected set; }
		public int MinHeight { get; protected set; }



		#region PROPERTIES
		﻿  ﻿
		/// <summary>
		/// Gets or sets the text.
		/// </summary>
		/// <value>The text.</value>
		public String Text 
		{	﻿  ﻿  ﻿
			get{ return PangoLayout.Text; }
			set
			{				﻿  ﻿  ﻿  ﻿
				Cairo.Rectangle rcBefore = GetTextArea();				﻿  ﻿  ﻿  ﻿
				PangoLayout.SetText( value );				﻿  ﻿  ﻿  ﻿
				Rectangle rcAfter = GetTextArea();				﻿  ﻿  ﻿  ﻿
				Rectangle rc = CairoUtils.RectangleUnion( rcBefore, rcAfter );
				﻿  ﻿  ﻿  ﻿
				if( !CairoUtils.IsRectangleNull( rc ) )		
				{
					QueueDrawArea( (int)rc.X, (int)rc.Y, (int)rc.Width, (int)rc.Height );
				}﻿
			}
		}

		/// <summary>
		/// Gets or sets the color of the text.
		/// </summary>
		/// <value>The color of the foreground.</value>
		public Gdk.Color TextColor 
		{			﻿  ﻿  ﻿
			get{ return _TextColor; }			﻿  ﻿  ﻿
			set
			{				﻿  ﻿  ﻿  ﻿
				Rectangle rc = GetTextArea();				﻿  ﻿  ﻿  ﻿
				_TextColor = value;				﻿  ﻿  ﻿  ﻿
				if( !CairoUtils.IsRectangleNull( rc ) )		
				{
					QueueDrawArea( (int)rc.X, (int)rc.Y, (int)rc.Width, (int)rc.Height );
				}﻿

			}﻿  ﻿  ﻿
		}
		﻿  ﻿
		/// <summary>
		/// Gets or sets the size of the font.
		/// </summary>
		/// <value>The size of the font.</value>
		public int FontSize 
		{			﻿  ﻿  ﻿
			get{ return PangoLayout.FontDescription.Size / (int)Pango.Scale.PangoScale; }			﻿  ﻿  ﻿
			set
			{				﻿  ﻿  ﻿  ﻿
				Rectangle rcBefore = GetTextArea();				﻿  ﻿  ﻿  ﻿
				PangoLayout.FontDescription.Size = value * (int)Pango.Scale.PangoScale;				﻿  ﻿  ﻿  ﻿
				Rectangle rcAfter = GetTextArea();				﻿  ﻿  ﻿  ﻿
				Rectangle rc = CairoUtils.RectangleUnion( rcBefore, rcAfter );
				﻿  ﻿  ﻿  ﻿
				if( !CairoUtils.IsRectangleNull( rc ))	
				{
					QueueDrawArea( (int)rc.X, (int)rc.Y, (int)rc.Width, (int)rc.Height );
				}﻿
			}
		}
		﻿  ﻿
		/// <summary>
		/// Gets or sets the font weight.
		/// </summary>
		/// <value>The font weight.</value>
		public Pango.Weight FontWeight 
		{			﻿  ﻿  ﻿
			get{ return PangoLayout.FontDescription.Weight; }			﻿  ﻿  ﻿
			set
			{				﻿  ﻿  ﻿  ﻿
				Rectangle rcBefore = GetTextArea();				﻿  ﻿  ﻿  ﻿
				PangoLayout.FontDescription.Weight = value;				﻿  ﻿  ﻿  ﻿
				Rectangle rcAfter = GetTextArea();				﻿  ﻿  ﻿  ﻿
				Rectangle rc = CairoUtils.RectangleUnion( rcBefore, rcAfter );				﻿  ﻿  ﻿  ﻿
				if( !CairoUtils.IsRectangleNull( rc ) )					
				{
					QueueDrawArea( (int)rc.X, (int)rc.Y, (int)rc.Width, (int)rc.Height );
				}﻿
			}
		}
		﻿  ﻿
		/// <summary>
		/// Gets or sets the font style.
		/// </summary>
		/// <value>The font style.</value>
		﻿public Pango.Style FontStyle 
		{			﻿  ﻿  ﻿
			get{ return PangoLayout.FontDescription.Style; }			﻿  ﻿  ﻿
			set
			{				﻿  ﻿  ﻿  ﻿
				Rectangle rcBefore = GetTextArea();				﻿  ﻿  ﻿  ﻿
				PangoLayout.FontDescription.Style = value;				﻿  ﻿  ﻿  ﻿
				Rectangle rcAfter = GetTextArea();				﻿  ﻿  ﻿  ﻿
				Rectangle rc = CairoUtils.RectangleUnion( rcBefore, rcAfter );
				﻿  ﻿  ﻿  ﻿
				if( !CairoUtils.IsRectangleNull( rc ) )		
				{
					QueueDrawArea( (int)rc.X, (int)rc.Y, (int)rc.Width, (int)rc.Height );
				}﻿
			}
		}
		﻿  ﻿
		/// <summary>
		/// Gets or sets the font.
		/// </summary>
		/// <value>The font.</value>
		public String Font 
		{
			get{ return PangoLayout.FontDescription.Family; }			﻿  ﻿   ﻿
			set
			{﻿				﻿  ﻿  ﻿  ﻿
				Rectangle rcBefore = GetTextArea();				﻿  ﻿  ﻿  ﻿
				PangoLayout.FontDescription.Family = value;				﻿  ﻿  ﻿  ﻿
				Rectangle rcAfter = GetTextArea();				﻿  ﻿  ﻿  ﻿
				Rectangle rc = CairoUtils.RectangleUnion( rcBefore, rcAfter );
				﻿  ﻿  ﻿  ﻿
				if( !CairoUtils.IsRectangleNull( rc ) )	
				{
					QueueDrawArea( (int)rc.X, (int)rc.Y, (int)rc.Width, (int)rc.Height );
				}﻿
			}
		}
				﻿  ﻿  
		/// <summary>
		/// Gets or sets the text align.
		/// </summary>
		/// <value>The text align.</value>
		public Align TextAlign 
		{			﻿  ﻿  ﻿
			get
			{				﻿  ﻿  ﻿  ﻿
				HorizontalAlign ha;				﻿  ﻿  ﻿  ﻿
				if( PangoLayout.Alignment == Pango.Alignment.Left )		
				{
					ha = HorizontalAlign.Left;
				}﻿  ﻿  ﻿  ﻿  ﻿
				else if( PangoLayout.Alignment == Pango.Alignment.Center )
				{
					ha = HorizontalAlign.Center;
				}
				else
				{
					ha = HorizontalAlign.Right;
				}
				return ﻿(Align)( (int)ha | (int)TextVerticalAlign );
			}			﻿  ﻿  ﻿
			set
			{				﻿  ﻿  ﻿  ﻿
				Rectangle rcBefore = GetTextArea();				﻿  ﻿  ﻿  ﻿
				if( (VerticalAlign)( (int)value & (int)VerticalAlign.Bottom ) == VerticalAlign.Bottom )
				{
					TextVerticalAlign = VerticalAlign.Bottom;
				}				﻿  ﻿  ﻿  ﻿
				else if( (VerticalAlign)( (int)value & (int)VerticalAlign.Middle ) == VerticalAlign.Middle )
				{
					TextVerticalAlign = VerticalAlign.Middle;
				}
				else
				{
					TextVerticalAlign = VerticalAlign.Top;
				}				﻿  ﻿  ﻿  ﻿
				﻿  ﻿  ﻿  ﻿
				if( (HorizontalAlign)( (int)value & (int)HorizontalAlign.Left ) == HorizontalAlign.Left )
				{
					PangoLayout.Alignment = Pango.Alignment.Left;
				}
				else if( (HorizontalAlign)( (int)value & (int)HorizontalAlign.Center ) == HorizontalAlign.Center )
				{
					PangoLayout.Alignment = Pango.Alignment.Center;
				}
				else
				{
					PangoLayout.Alignment = Pango.Alignment.Right;
				}		

				Rectangle rcAfter = GetTextArea();				﻿  ﻿  ﻿  ﻿
				Rectangle rc = CairoUtils.RectangleUnion( rcBefore, rcAfter );
				﻿  ﻿  ﻿  ﻿
				if( !CairoUtils.IsRectangleNull( rc ) )
				{
					QueueDrawArea( (int)rc.X, (int)rc.Y, (int)rc.Width, (int)rc.Height );
				}				﻿  ﻿  ﻿  ﻿
			}
		}

		/// <summary>
		/// Gets or sets the image.*/
		/// </summary>
		/// <value>The image.</value>
		public Gdk.Pixbuf Icon 
		{		﻿  ﻿  ﻿
			get
			{
				return _Icon;
			}			﻿  ﻿  ﻿
			set
			{
				Rectangle rcBefore = GetImageArea();				﻿  ﻿  ﻿  ﻿
				_Icon = value;				﻿  ﻿  ﻿  ﻿
				Rectangle rcAfter = GetImageArea();				﻿  ﻿  ﻿  ﻿
				Rectangle rc = CairoUtils.RectangleUnion( rcBefore, rcAfter );
				﻿  ﻿  ﻿  ﻿
				if( !CairoUtils.IsRectangleNull( rc ) )
				{
					QueueDrawArea( (int)rc.X, (int)rc.Y, (int)rc.Width, (int)rc.Height );
				}
			}
		}
		﻿  ﻿
		/// <summary>
		/// Gets or sets the image align.
		/// </summary>
		/// <value>The image align.</value>
		﻿public Align IconAlign 
		{
			get{ return _IconAlign; }			﻿  ﻿  ﻿
			set
			{
				Rectangle rcBefore = GetImageArea();
				_IconAlign = value;				﻿  ﻿  ﻿  ﻿
				Rectangle rcAfter = GetImageArea();
				Rectangle rc = CairoUtils.RectangleUnion( rcBefore, rcAfter );
				﻿  ﻿  ﻿  ﻿
				if( !CairoUtils.IsRectangleNull( rc ) )
				{
					QueueDrawArea( (int)rc.X, (int)rc.Y, (int)rc.Width, (int)rc.Height );
				}
			}
		}
		﻿  ﻿
		﻿#endregion
		﻿  ﻿




		// CTOR / DISPOSAL
		// -------------------------------------------------------------------------------------------------------

		/// <summary>
		/// Initializes a new instance of the <see cref="Muvimo.Toolkit.UI.GtkSharp.ButtonBase"/> class.
		/// </summary>
		public ButtonBase()
		{
			Init();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Muvimo.Toolkit.UI.GtkSharp.ButtonBase"/> class and sets the passed text.
		/// </summary>
		/// <param name="text">Text.</param>
		public ButtonBase( string text )
		{
			Init();
			this.Text = text;
		}

		﻿/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="Muvimo.Toolkit.UI.GtkSharp.ButtonBase"/>.
		/// The <see cref="Dispose"/> method leaves the <see cref="Muvimo.Toolkit.UI.GtkSharp.ButtonBase"/> in an unusable
		/// state. After calling <see cref="Dispose"/>, you must release all references to the
		/// <see cref="Muvimo.Toolkit.UI.GtkSharp.ButtonBase"/> so the garbage collector can reclaim the memory that the
		/// <see cref="Muvimo.Toolkit.UI.GtkSharp.ButtonBase"/> was occupying.</remarks>
		public override void Dispose()
		{
			if(PangoLayout != null )PangoLayout.Dispose();
			if(_Icon != null ) _Icon.Dispose();

			base.Dispose();
		}



		// PRIVATE
		// -------------------------------------------------------------------------------------------------------

		/// <summary>
		/// Initializes this instance.
		/// </summary>
		private void Init()
		{
			this.Events = Gdk.EventMask.ButtonPressMask |﻿  ﻿  ﻿  ﻿
				Gdk.EventMask.ButtonReleaseMask |﻿
				Gdk.EventMask.KeyPressMask |﻿
				Gdk.EventMask.KeyReleaseMask |
				Gdk.EventMask.EnterNotifyMask |
				Gdk.EventMask.LeaveNotifyMask |
				Gdk.EventMask.FocusChangeMask;
			﻿  ﻿  ﻿  			﻿  ﻿  ﻿
			CanFocus = true;

			MinWidth = 120;
			MinHeight = 40;
			﻿  ﻿  ﻿
			PangoLayout = new Pango.Layout( this.CreatePangoContext() );			﻿  ﻿  ﻿
			PangoLayout.Wrap = Pango.WrapMode.WordChar;			﻿  ﻿  ﻿
			PangoLayout.FontDescription = new Pango.FontDescription();			﻿  ﻿  ﻿
			PangoLayout.Alignment = Pango.Alignment.Center;			﻿  ﻿  ﻿
			PangoLayout.FontDescription.Family = "Calibri";			﻿  ﻿  ﻿
			PangoLayout.FontDescription.Size = 10 * (int)Pango.Scale.PangoScale;
		}



		// OVERRIDES
		// -------------------------------------------------------------------------------------------------------

		/// <summary>
		/// Raises the button press event event.
		/// </summary>
		/// <param name="ev">Ev.</param>
		﻿protected override bool OnButtonPressEvent( Gdk.EventButton ev )
		{			﻿  ﻿  ﻿
			this.GrabFocus();
			if( ev.Button == 1 )
			{				﻿  ﻿  ﻿  ﻿
				IsMouseDown = true;				﻿  ﻿  ﻿  ﻿
				QueueDraw();﻿
				﻿  ﻿  ﻿  ﻿
				if( Pressed != null )		
				{
					Pressed( this, new EventArgs() );
				}﻿
			}			﻿  ﻿
			return base.OnButtonPressEvent( ev );
		}

		﻿protected override bool OnButtonReleaseEvent( Gdk.EventButton ev )
		{			﻿  ﻿  ﻿
			if( ev.Button == 1 )
			{				﻿  ﻿  ﻿  ﻿
				IsMouseDown = false;				﻿  ﻿  ﻿  ﻿
				QueueDraw();﻿
				﻿  ﻿  ﻿  ﻿
				if( Released != null )			
				{
					Released( this, new EventArgs() );
				}
			}			﻿  ﻿  ﻿
			return base.OnButtonReleaseEvent( ev );
		}
		﻿  ﻿
		﻿protected override bool OnKeyPressEvent( Gdk.EventKey evnt )
		{
			﻿  ﻿  ﻿
			if( evnt.Key == Gdk.Key.space || evnt.Key == Gdk.Key.Return || evnt.Key == Gdk.Key.KP_Enter )
			{
				﻿  ﻿  ﻿  ﻿
				IsMouseDown = true;				﻿  ﻿  ﻿  ﻿
				QueueDraw();﻿
				﻿  ﻿  ﻿  ﻿
				if( Pressed != null )		
				{
					Pressed( this, new EventArgs() );
				}﻿
			}			﻿  ﻿  ﻿
			return base.OnKeyPressEvent( evnt );
		}

		﻿protected override bool OnKeyReleaseEvent( Gdk.EventKey evnt )
		{
			﻿  ﻿  ﻿
			if( evnt.Key == Gdk.Key.space || evnt.Key == Gdk.Key.Return || evnt.Key == Gdk.Key.KP_Enter )
			{				﻿  ﻿  ﻿  ﻿
				IsMouseDown = false;				﻿  ﻿  ﻿  ﻿
				QueueDraw();﻿
				﻿  ﻿  ﻿  ﻿
				if( Released != null )		
				{
					Released( this, new EventArgs() );
				}﻿
			}			﻿  ﻿  ﻿
			return base.OnKeyReleaseEvent( evnt );
		}
		﻿  ﻿
		﻿protected override bool OnEnterNotifyEvent( Gdk.EventCrossing evnt )
		{			﻿  ﻿  ﻿
			IsMouseOver = true;
			QueueDraw();			﻿  ﻿  ﻿
			return base.OnEnterNotifyEvent( evnt );
		}

		﻿protected override bool OnLeaveNotifyEvent( Gdk.EventCrossing evnt )
		{			﻿  ﻿  ﻿
			IsMouseOver = false;			﻿  ﻿  ﻿
			QueueDraw();			﻿  ﻿  ﻿
			return base.OnLeaveNotifyEvent( evnt );
		}

		protected override bool OnExposeEvent( Gdk.EventExpose ev )
		{			﻿  ﻿  ﻿
			Cairo.Context cr = Gdk.CairoHelper.Create( ev.Window );			﻿  ﻿  ﻿
			DrawBackground( cr, ev );			﻿  ﻿  ﻿
			DrawBorder( cr, ev );			﻿  ﻿  ﻿
			DrawImage( cr, ev );			﻿  ﻿  ﻿
			DrawText( cr, ev );
			﻿  ﻿  ﻿
			( (IDisposable)cr.Target ).Dispose();			﻿
			( (IDisposable)cr ).Dispose();			﻿  ﻿  ﻿
			return true;
		}
		﻿  ﻿
		/// <summary>
		/// Called when resize was requested
		/// </summary>
		/// <param name="requisition">Requisition.</param>
		protected override void OnSizeRequested( ref Gtk.Requisition requisition )
		{
			// Calculate desired size here.
			requisition.Width = MinWidth;
			requisition.Height = MinHeight;
		}



		// VIRTUALS ( for overriding )
		// -------------------------------------------------------------------------------------------------------

		﻿protected virtual void DrawBackground( Cairo.Context cr, Gdk.EventExpose ev )
		{
		}
		﻿  ﻿
		protected virtual void DrawBorder( Cairo.Context cr, Gdk.EventExpose ev )
		{
		}
		﻿  ﻿
		﻿protected virtual void DrawImage( Cairo.Context cr, Gdk.EventExpose ev )
		{
		}
		﻿  ﻿
		protected virtual void DrawText( Cairo.Context cr, Gdk.EventExpose ev )
		{
		}

﻿  ﻿  // <summary>
﻿  ﻿  /// Calculates text position, which depends on TextAllign
﻿  ﻿  /// </summary>
﻿  ﻿  /// <returns>
﻿  ﻿  /// Uper left point of text.
﻿  ﻿  /// A <see cref="Point"/>
﻿  ﻿  /// </returns>
﻿  ﻿  protected virtual Cairo.Point GetTextPosition()
		{			﻿  ﻿  ﻿
			int h, w;			﻿  ﻿  ﻿
			PangoLayout.GetPixelSize( out w, out h );			﻿  ﻿  ﻿
			if( TextVerticalAlign == VerticalAlign.Bottom )
			{
				h = this.Allocation.Height - h;			﻿  ﻿  ﻿
			}
			else if( TextVerticalAlign == VerticalAlign.Middle )
			{
				h = this.Allocation.Height / 2 - h / 2;
			}			﻿  ﻿  ﻿
			else
			{
				h = 0;
			}			﻿  ﻿  ﻿
			return new Cairo.Point( 0, h );
		}
		﻿  ﻿
﻿  ﻿  /// <summary>
﻿  ﻿  /// Calculates area where text is drawn
﻿  ﻿  /// </summary>
﻿  ﻿  /// <returns>
﻿  ﻿  /// A <see cref="Rectangle"/>
﻿  ﻿  /// </returns>
﻿  ﻿  protected virtual Rectangle GetTextArea()
		{		﻿  ﻿  ﻿

			int h, w;			﻿  ﻿  ﻿
			int y;			﻿  ﻿  ﻿
			Pango.Rectangle inkRect;			﻿  ﻿  ﻿
			Pango.Rectangle logRect;			﻿  ﻿  ﻿

			PangoLayout.GetPixelExtents( out inkRect, out logRect );			﻿  ﻿  ﻿
			PangoLayout.GetPixelSize( out w, out h );
			﻿  ﻿  ﻿
			if( TextVerticalAlign == VerticalAlign.Bottom )
			{
				y = this.Allocation.Height - h;
			}			﻿  ﻿  ﻿
			else if( TextVerticalAlign == VerticalAlign.Middle )
			{
				y = this.Allocation.Height / 2 - h / 2;
			}			﻿  ﻿  ﻿
			else
			{
				y = 0;
			}﻿
			return new Rectangle( logRect.X, y, w, h );
		}
		﻿  ﻿
		protected virtual Point GetImagePosition()
		{			﻿  ﻿  ﻿
			int x = 0;			﻿  ﻿  ﻿
			int y = 0;﻿			﻿  ﻿  ﻿
			int imageOffset = 3;		﻿  ﻿  ﻿
			﻿  ﻿  ﻿
			if( _Icon != null )
			{
				﻿  ﻿  ﻿  ﻿
				if( (VerticalAlign)( (int)IconAlign & (int)VerticalAlign.Bottom ) == VerticalAlign.Bottom )
				{
					y = this.Allocation.Height - imageOffset - _Icon.Height;
				}				﻿  ﻿  ﻿  ﻿
				else if( (VerticalAlign)( (int)IconAlign & (int)VerticalAlign.Middle ) == VerticalAlign.Middle )
				{
					y = this.Allocation.Height / 2 - this._Icon.Height / 2;
				}
				else
				{
					y = imageOffset;
				}				﻿  ﻿  ﻿  ﻿  ﻿  ﻿
				﻿  ﻿  ﻿  ﻿
				if( (HorizontalAlign)( (int)IconAlign & (int)HorizontalAlign.Center ) == HorizontalAlign.Center )
				{
					x = this.Allocation.Width / 2 - this._Icon.Width / 2;
				}				﻿  ﻿  ﻿  ﻿
				else if( (HorizontalAlign)( (int)IconAlign & (int)HorizontalAlign.Right ) == HorizontalAlign.Right )
				{
					x = this.Allocation.Width - imageOffset - this._Icon.Width;
				}
				else
				{
					x = imageOffset;
				}
			}			﻿  ﻿  ﻿
			return new Point( x, y );
		}
		﻿  ﻿
		﻿protected virtual Rectangle GetImageArea()
		{			﻿  ﻿  ﻿
			Rectangle rect;			﻿  ﻿  ﻿
			if( _Icon != null )
			{				﻿  ﻿  ﻿  ﻿
				Point pt = GetImagePosition();				﻿  ﻿  ﻿  ﻿
				rect = new Rectangle( pt.X, pt.Y, _Icon.Width, _Icon.Height );
			}			﻿  ﻿  ﻿
			else
			{
				rect = new Rectangle( 0, 0, 0, 0 );
			}			﻿  ﻿  ﻿
			return rect;﻿  ﻿
		}
		﻿  ﻿
		﻿
	}
}
